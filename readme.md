# MosMaSys

MosMaSys or Mosque Management System is an application to help mosque caretaker for handle some problem, for example :

 - Financial Management like :
    1. Calculation for Alms, Infaq, Alms Expenditure, and Infaq Expenditure.
    2. Monthly and yearly reports.
 - Pilgrims data (with number of active pilgrims and, number of residents around).
 - Manage Asset and Mosque facilities.
 - Mosque Activities.
 - Manage Friday's Khatib Agenda.
 - Manage Ramadhan Activities.

## Tech

MosMaSys uses some of open sources projects to work properly :

 - [Laravel](https://laravel.com/) - PHP Framework for Web Artisans!
 - [Core UI](https://coreui.io/) - Bootstrap Admin Template
 - [Composer](https://getcomposer.org/) - Dependency Manager for PHP
 - [JQuery](https://jquery.com/) - Javascript Library
 - [Visual Studio Code](https://code.visualstudio.com/) - Text Editor with cool extensions to easier code for developer 

## Fork it!

Fork me on :

 - [Github](https://github.com/)
 - [Bitbucket](https://bitbucket.org/Ulsyairil/mosmasys/src/master/)
 - [Gitlab](https://about.gitlab.com/)

## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)
- That's it: launch the main URL or go to __/login__ and login with default credentials __admin@admin.com__ - __password__

## License

Basically, feel free to use and re-use any way you want. 
