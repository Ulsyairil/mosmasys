<?php

return [
    'date_format'         => 'd F Y',
    'time_format'         => 'H:i',
    'primary_language'    => 'en',
    'available_languages' => [
        'en' => 'English',
        'id' => 'Indonesian'
    ],
];
